﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SigobMetas.Microcharts;
using SigobMetas.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;
using SigobMetas.Views;


namespace SigobMetas.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Metas : ContentPage
	{
        MetasViewModel viewModel;
        Microcharts_Data data = new Microcharts_Data();
        public Metas ()
		{
			InitializeComponent ();
            BindingContext = viewModel = new MetasViewModel();
            EnGestion.Chart = new RadialGaugeChart { Entries = data.GetEnGestion() };
            Atrasadas.Chart = new RadialGaugeChart { Entries = data.GetAtrasadas() };
            Detenidas.Chart = new RadialGaugeChart { Entries = data.GetDetenidas() };
            Terminadas.Chart = new RadialGaugeChart { Entries = data.GetTerminadas() };
            Barras.Chart = new BarChart() { Entries = data.GetBarras() };
            MapView.MapType = MapType.Street;
            MapView.MoveToRegion(
                MapSpan.FromCenterAndRadius(
                new Position(4, -72), 
                Distance.FromMiles(1)));
           
        }
        private void Street_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Street;
        }


        private void Hybrid_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Hybrid;
        }

        private void Satellite_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Satellite;
        }
        private async void OnLabelClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ItemsPage());
        }
    }
}